# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
}

# aws_iot_policy pub-sub
resource "aws_iot_policy" "pub-sub" {
  name = "PubSubToAnyTopic_GNV"

  policy = jsonencode({
    Version: "2012-10-17",
    Statement: [
        {
        Action: [
            "iot:Connect"
        ],
        Effect: "Allow",
        Resource: "*"
        },
        {
        Action: [
            "iot:Receive"
        ],
        Effect: "Allow",
        Resource: "*"
        },
        {
        Action: [
            "iot:Publish"
        ],
        Effect: "Allow",
        Resource: "*"
        },
        {
        Action: [
            "iot:Subscribe"
        ],
        Effect: "Allow",
        Resource: "*"
        }
    ]
  })
}

# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "attachment" {
  policy = aws_iot_policy.pub-sub.name
  target = aws_iot_certificate.cert.arn
}

# aws_iot_thing temp_sensor
resource "aws_iot_thing" "temp_sensor" {
  name = "example"

  attributes = {
    First = "examplevalue"
  }
}

# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "temperature_rule" {
  name = "TemperatureRule"
  description = "Example rule"
  enabled = true
  sql = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.temperature-table.name
    }
  }
}

# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "rule" {
  name        = "MyRule"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

    timestream {
        role_arn = aws_iam_role.iot_role.arn
        table_name = "${aws_timestreamwrite_table.example.table_name}"
        database_name = "${aws_timestreamwrite_database.example.database_name}"
        dimension {
            name  = "sensor_id"
            value = "$${sensor_id}"
        }

        dimension {
            name  = "temperature"
            value = "$${temperature}"
        }

        dimension {
            name  = "zone_id"
            value = "$${zone_id}"
        }

        timestamp {
            unit  = "MILLISECONDS"
            value = "$${timestamp()}"
        }
    }

}


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
